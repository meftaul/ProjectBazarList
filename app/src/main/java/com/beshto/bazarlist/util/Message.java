package com.beshto.bazarlist.util;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by lenovo_pc on 7/19/2016.
 */
public class Message {

    public static void message(Context ctx, String message){
        Toast.makeText(ctx, message, Toast.LENGTH_SHORT).show();
    }
}
