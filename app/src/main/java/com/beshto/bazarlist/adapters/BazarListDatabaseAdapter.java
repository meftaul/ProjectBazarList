package com.beshto.bazarlist.adapters;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.beshto.bazarlist.entity.Category;
import com.beshto.bazarlist.entity.Item;
import com.beshto.bazarlist.util.Message;

import java.util.ArrayList;


public class BazarListDatabaseAdapter {

    BazarListDatabaseHelper dbHelper;
    public BazarListDatabaseAdapter(Context context){
        dbHelper = new BazarListDatabaseHelper(context);
    }


    public boolean isCategory(long categoryId){

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String[] columns = {dbHelper.CATEGORY_ID, dbHelper.CATEGORY_NAME};
        Cursor cursor = db.query(dbHelper.TABLE_NAME,columns, dbHelper.CATEGORY_ID+" = "+categoryId+" ", null, null, null, null);
        int rowCount = cursor.getCount();
        cursor.close();
        return rowCount == 1;
    }

    public long insertCategory(Category category){
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(dbHelper.CATEGORY_NAME, category.getCategoryName());

        long id = db.insert(dbHelper.TABLE_NAME, null, contentValues);

        return id;
    }

    public ArrayList<Category> getAllCategory(){
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String[] columns = {dbHelper.CATEGORY_ID, dbHelper.CATEGORY_NAME};
        Cursor cursor = db.query(dbHelper.TABLE_NAME,columns, null, null, null, null, dbHelper.CATEGORY_ID+" DESC");

        ArrayList<Category> categories = new ArrayList<Category>();

        while (cursor.moveToNext()){

            int index1 = cursor.getColumnIndex(dbHelper.CATEGORY_ID);
            int index2 = cursor.getColumnIndex(dbHelper.CATEGORY_NAME);

            Category category = new Category();
            category.setId(cursor.getLong(index1));
            category.setCategoryName(cursor.getString(index2));

            categories.add(category);
        }
        cursor.close();

        return categories;
    }

    public  ArrayList<Category> getCategoriesByName(String categoryName){

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String[] columns = {dbHelper.CATEGORY_ID, dbHelper.CATEGORY_NAME};
        Cursor cursor = db.query(dbHelper.TABLE_NAME,columns, dbHelper.CATEGORY_NAME+" = '"+categoryName+"' ", null, null, null, null);

        ArrayList<Category> categories = new ArrayList<Category>();

        while (cursor.moveToNext()){

            int index1 = cursor.getColumnIndex(dbHelper.CATEGORY_ID);
            int index2 = cursor.getColumnIndex(dbHelper.CATEGORY_NAME);

            Category category = new Category();
            category.setId(cursor.getLong(index1));
            category.setCategoryName(cursor.getString(index2));

            categories.add(category);
        }
        cursor.close();

        return categories;
    }

    public long deleteCategory(long categoryId){
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String[] whereArgs = {String.valueOf(categoryId)};
        int count = db.delete(dbHelper.TABLE_NAME, dbHelper.CATEGORY_ID+" = ? ", whereArgs);

        return count;
    }

    public long insertItem(Item item){

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(dbHelper.ITEM_CATEGORY_ID, item.getCategoryId());
        contentValues.put(dbHelper.ITEM_NAME, item.getItemName());
        contentValues.put(dbHelper.ITEM_STATUS, item.isStatus());

        long itemId = db.insert(dbHelper.ITEM_TABLE, null, contentValues);

        return itemId;
    }

    public ArrayList<Item> getItemsByCategoryId(long categoryId){

        if (!isCategory(categoryId)){
            return null;
        }

        ArrayList<Item> items = new ArrayList<Item>();

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String[] columns = {dbHelper.ITEM_ID, dbHelper.ITEM_NAME, dbHelper.ITEM_STATUS, dbHelper.ITEM_CATEGORY_ID};
        Cursor cursor = db.query(dbHelper.ITEM_TABLE,columns, dbHelper.ITEM_CATEGORY_ID+" = "+categoryId+" ", null, null, null, null);

        while (cursor.moveToNext()){
            int itemIdIndex = cursor.getColumnIndex(dbHelper.ITEM_ID);
            int itemCategoryIndex = cursor.getColumnIndex(dbHelper.ITEM_CATEGORY_ID);
            int itemNameIndex = cursor.getColumnIndex(dbHelper.ITEM_NAME);
            int itemStatusIndex = cursor.getColumnIndex(dbHelper.ITEM_STATUS);

            Item newItem = new Item();
            newItem.setItemId(cursor.getLong(itemIdIndex));
            newItem.setCategoryId(cursor.getLong(itemCategoryIndex));
            newItem.setItemName(cursor.getString(itemNameIndex));
            newItem.setStatus(cursor.getInt(itemStatusIndex) == 0);

            items.add(newItem);

        }

        return items;
    }

    public long deleteItem(long itemId){

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        String[] whereArgs = {String.valueOf(itemId)};
        int count = db.delete(dbHelper.ITEM_TABLE, dbHelper.ITEM_ID+" = ? ", whereArgs);

        Log.i("DB HELPER","Item deleted: "+count);

        return count;
    }

    static class BazarListDatabaseHelper extends SQLiteOpenHelper {
        private static final String DATABASE_NAME="bazarlist.db";
        private static final int DATABASE_VRSION=12;

        private static final String TABLE_NAME="category";
        private static final String CATEGORY_ID="_id";
        private static final String CATEGORY_NAME="category_name";
        private static final String CREATE_CATEGORY_TABLE="CREATE TABLE "+TABLE_NAME+" ("+CATEGORY_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+CATEGORY_NAME+" VARCHAR(255));";
        private static final String DROP_CATEGORY_TABLE="DROP TABLE IF EXISTS "+TABLE_NAME;


        private static final String ITEM_TABLE = "item";
        private static final String ITEM_ID = "_id";
        private static final String ITEM_NAME = "item_name";
        private static final String ITEM_STATUS = "status";
        private static final String ITEM_CATEGORY_ID = "category_id";

        private static final String CREATE_ITEM_TABLE = "CREATE TABLE "+ITEM_TABLE+" (" +
                                                            ITEM_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, " +
                                                            ITEM_CATEGORY_ID+" INTEGER, "+
                                                            ITEM_NAME+"  VARCHAR(255), " +
                                                            ITEM_STATUS+" INTEGER NOT NULL DEFAULT 0)";
        private static final String DROP_ITEM_TABLE = "DROP TABLE IF EXISTS "+ITEM_TABLE;


        private Context ctx;

        public BazarListDatabaseHelper(Context context){
            super(context, DATABASE_NAME, null, DATABASE_VRSION);
            this.ctx = context;
            //Message.message(context, "Constructor called");
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

            try {
                db.execSQL(CREATE_CATEGORY_TABLE);
                db.execSQL(CREATE_ITEM_TABLE);
                Message.message(ctx, "onCreate called");
            }catch(SQLException e){
                e.printStackTrace();
                //Message.message(ctx, ""+e);
            }

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

            db.execSQL(DROP_CATEGORY_TABLE);
            db.execSQL(DROP_ITEM_TABLE);
            onCreate(db);
            //Message.message(ctx, "onUpgrade called");

        }
    }
}
