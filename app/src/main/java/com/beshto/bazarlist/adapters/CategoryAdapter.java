package com.beshto.bazarlist.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.beshto.bazarlist.CategoryItem;
import com.beshto.bazarlist.MainActivity;
import com.beshto.bazarlist.R;
import com.beshto.bazarlist.entity.Category;
import com.beshto.bazarlist.util.Message;

import java.util.ArrayList;


public class CategoryAdapter extends BaseAdapter {

    ArrayList<Category> categoryArrayList;
    Context context;

    private static LayoutInflater inflater = null;

    public CategoryAdapter(MainActivity mainActivity, ArrayList<Category> categories){
        categoryArrayList = categories;
        context = mainActivity;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return categoryArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return categoryArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return categoryArrayList.get(position).getId();
    }

    public void addNewCategory(Category category){
        categoryArrayList.add(0, category);
        notifyDataSetChanged();
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = new ViewHolder();
        View rowView = inflater.inflate(R.layout.category_listview, null);
        holder.categoryNameTv = (TextView) rowView.findViewById(R.id.categoryName);
        holder.getCategoryNameId = (TextView) rowView.findViewById(R.id.categoryId);

        holder.categoryNameTv.setText(categoryArrayList.get(i).getCategoryName());
        holder.getCategoryNameId.setText(String.valueOf(categoryArrayList.get(i).getId()));

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Message.message(context, "You Have clicked "+getItemId(i));

                Intent intent = new Intent(context, CategoryItem.class);
                intent.putExtra("categoryId", String.valueOf(getItemId(i)));
                intent.putExtra("categoryName", getItem(i).toString());
                context.startActivity(intent);

            }
        });

        return rowView;
    }

    public class ViewHolder{
        TextView categoryNameTv, getCategoryNameId;
    }
}
