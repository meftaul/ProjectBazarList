package com.beshto.bazarlist.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.beshto.bazarlist.CategoryItem;
import com.beshto.bazarlist.R;
import com.beshto.bazarlist.entity.Category;
import com.beshto.bazarlist.entity.Item;
import com.beshto.bazarlist.util.Message;

import java.util.ArrayList;


public class ItemAdapter extends BaseAdapter {

    ArrayList<Item> itemArrayList;
    Context context;
    private static LayoutInflater inflater = null;

    public ItemAdapter(CategoryItem categoryItemActivity, ArrayList<Item> items){
        itemArrayList = items;
        context = categoryItemActivity;
        inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return itemArrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return itemArrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return itemArrayList.get(i).getItemId();
    }

    public void addNewCategory(Item item){
        itemArrayList.add(0, item);
        notifyDataSetChanged();
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        ViewHolder holder = new ViewHolder();
        View rowView = inflater.inflate(R.layout.item_listview, null);

        holder.itemName = (TextView) rowView.findViewById(R.id.itemName);
        holder.statusButton = (ImageButton) rowView.findViewById(R.id.itemStatus);
        holder.deleteButton = (ImageButton) rowView.findViewById(R.id.itemDelete);

        holder.itemName.setText(itemArrayList.get(i).getItemName());
        //holder.statusButton.setImageResource(R.drawable.);

        /*rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Message.message(context, "You Have clicked "+getItemId(i));
            }
        });*/

        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new BazarListDatabaseAdapter(context).deleteItem(getItemId(i));
                Message.message(context, "Item deleted");
                itemArrayList.remove(i);
                notifyDataSetChanged();
            }
        });


        return rowView;
    }

    public class ViewHolder{
        TextView itemName;
        ImageButton statusButton, deleteButton;
    }
}
