package com.beshto.bazarlist;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.beshto.bazarlist.adapters.BazarListDatabaseAdapter;
import com.beshto.bazarlist.adapters.CategoryAdapter;
import com.beshto.bazarlist.entity.Category;
import com.beshto.bazarlist.entity.Item;
import com.beshto.bazarlist.util.Message;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    BazarListDatabaseAdapter dbAdapter;
    CategoryAdapter categoryListAdapter;
    EditText categoryEditText;
    Button addCategoryButton;
    ListView categoryListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        categoryEditText = (EditText) findViewById(R.id.category);
        addCategoryButton = (Button) findViewById(R.id.addCategory);
        categoryListView = (ListView) findViewById(R.id.categoryListView);

        dbAdapter = new BazarListDatabaseAdapter(this);
        categoryListAdapter = new CategoryAdapter(this, dbAdapter.getAllCategory());
        categoryListView.setAdapter(categoryListAdapter);

    }

    public void addNewCategory(View view){

        if (categoryEditText.getText().toString().equals("")){
            Message.message(this, "Please insert list name");
            return;
        }

        Category newCategory = new Category();
        newCategory.setCategoryName(categoryEditText.getText().toString());

        long newCategoryId = dbAdapter.insertCategory(newCategory);
        if (newCategoryId > 0){
            Message.message(this, "New Category is Created");
            categoryEditText.getText().clear();
            newCategory.setId(newCategoryId);
            categoryListAdapter.addNewCategory(newCategory);
        }else{
            Message.message(this, "Category Creation failed");
        }

    }


    public void addItem(){
        Item item = new Item(2, "Item 01");
        dbAdapter.insertItem(item);
    }

    public void displayItems(){

        //dbAdapter.deleteItem(3);

        ArrayList<Item> items = dbAdapter.getItemsByCategoryId(2);
        for (Item item:items){
            Log.i("ITEM", "Item "+item);
        }

    }

    public void getAllCategories(){
        /*ArrayList<Category> categories = dbAdapter.getCategoriesByName("Category 03");
        for (Category category: categories){
            Log.i("Category", "Byname");
            Log.i("Category", "Id: "+category.getId()+": "+category.getCategoryName());
        }*/


        //dbAdapter.deleteCategory(5);

        Log.i("Category", "IS A CATEGORY? "+String.valueOf(dbAdapter.isCategory(2)));
        Log.i("Category", "IS A CATEGORY? "+String.valueOf(dbAdapter.isCategory(10)));

        ArrayList<Category> categories2 = dbAdapter.getAllCategory();
        for (Category category: categories2){
            Log.i("Category  ", "Id: "+category.getId()+": "+category.getCategoryName());
        }

    }
}
