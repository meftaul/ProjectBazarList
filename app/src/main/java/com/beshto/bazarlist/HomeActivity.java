package com.beshto.bazarlist;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

/**
 * Created by lenovo_pc on 7/20/2016.
 */
public class HomeActivity extends Activity {
    private CallbackManager callbackManager;
    private TextView info;
    private LoginButton loginButton;

//
//    Button btnSignIn,btnSignUp;
//    LoginDataBaseAdapter loginDataBaseAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        setContentView(R.layout.main);
        info = (TextView)findViewById(R.id.info);
        loginButton = (LoginButton)findViewById(R.id.login_button);

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                info.setText("User ID:  " +
                        loginResult.getAccessToken().getUserId() + "\n" +
                        "Auth Token: " + loginResult.getAccessToken().getToken());

                Intent intent = new Intent(HomeActivity.this, MainActivity.class);
                startActivity(intent);
            }

            @Override
            public void onCancel() {
                info.setText("Login attempt cancelled.");
            }

            @Override
            public void onError(FacebookException e) {
                info.setText("Login attempt failed.");
            }
        });

//
//
//
//        // create a instance of SQLite Database
//        loginDataBaseAdapter=new LoginDataBaseAdapter(this);
//        loginDataBaseAdapter=loginDataBaseAdapter.open();
//
//        // Get The Refference Of Buttons
//        btnSignIn=(Button)findViewById(R.id.buttonSignIN);
//        btnSignUp=(Button)findViewById(R.id.buttonSignUP);
//
//        // Set OnClick Listener on SignUp button
//        btnSignUp.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//
//                /// Create Intent for SignUpActivity  and Start The Activity
//                Intent intentSignUP=new Intent(getApplicationContext(),SignUPActivity.class);
//                startActivity(intentSignUP);
//            }
//        });
    }
    // Methos to handleClick Event of Sign In Button
//    public void signIn(View V)
//    {
//        final Dialog dialog = new Dialog(HomeActivity.this);
//        dialog.setContentView(R.layout.login);
//        dialog.setTitle("Login");
//
//        // get the Refferences of views
//        final EditText editTextUserName=(EditText)dialog.findViewById(R.id.editTextUserNameToLogin);
//        final  EditText editTextPassword=(EditText)dialog.findViewById(R.id.editTextPasswordToLogin);
//
//        Button btnSignIn=(Button)dialog.findViewById(R.id.buttonSignIn);
//
//        // Set On ClickListener
//        btnSignIn.setOnClickListener(new View.OnClickListener() {
//
//            public void onClick(View v) {
//                // get The User name and Password
//                String userName=editTextUserName.getText().toString();
//                String password=editTextPassword.getText().toString();
//
//                // fetch the Password form database for respective user name
//                String storedPassword=loginDataBaseAdapter.getSinlgeEntry(userName);
//
//                // check if the Stored password matches with  Password entered by user
//                if(password.equals(storedPassword))
//                {
//                    Toast.makeText(HomeActivity.this, "Congrats: Login Successfull", Toast.LENGTH_LONG).show();
//                    dialog.dismiss();
//                }
//                else
//                {
//                    Toast.makeText(HomeActivity.this, "User Name or Password does not match", Toast.LENGTH_LONG).show();
//                }
//            }
//        });
//
//        dialog.show();
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        // Close The Database
//        loginDataBaseAdapter.close();
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void useThisApp(View view) {
        Intent intent = new Intent(HomeActivity.this, MainActivity.class);
        startActivity(intent);
    }
}
