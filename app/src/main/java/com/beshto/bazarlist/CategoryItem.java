package com.beshto.bazarlist;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.beshto.bazarlist.adapters.BazarListDatabaseAdapter;
import com.beshto.bazarlist.adapters.ItemAdapter;
import com.beshto.bazarlist.entity.Item;
import com.beshto.bazarlist.util.Message;

public class CategoryItem extends AppCompatActivity {

    ListView itemListView;
    EditText itemNameEditText;
    Button addNewItemButton;

    BazarListDatabaseAdapter dbAdapter;
    ItemAdapter itemAdapter;
    private long categoryId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_item);
        dbAdapter = new BazarListDatabaseAdapter(this);

        itemListView = (ListView) findViewById(R.id.itemListView);
        itemNameEditText = (EditText) findViewById(R.id.itemNameText);
        addNewItemButton = (Button) findViewById(R.id.addNewItemButton);

        Intent intent = getIntent();
        categoryId = Long.valueOf(intent.getStringExtra("categoryId").toString());
        setTitle(intent.getStringExtra("categoryName"));

        Message.message(this, "Item"+categoryId);

        if(categoryId > 0){
            itemAdapter = new ItemAdapter(this, dbAdapter.getItemsByCategoryId(categoryId));
            itemListView.setAdapter(itemAdapter);
        }
    }

    public void addNewItem(View view){

        if (itemNameEditText.getText().toString().equals("")){
            Message.message(this, "Please insert list name");
            return;
        }

        Item newItem = new Item();
        newItem.setItemName(itemNameEditText.getText().toString());
        newItem.setCategoryId(categoryId);

        long newItemId = dbAdapter.insertItem(newItem);

        if (newItemId > 0){
            newItem.setItemId(newItemId);
            itemAdapter.addNewCategory(newItem);
            itemNameEditText.getText().clear();
        }else{
            Message.message(this, "Item Creation failed");
        }
    }
}
