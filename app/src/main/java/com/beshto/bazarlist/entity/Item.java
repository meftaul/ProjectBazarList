package com.beshto.bazarlist.entity;

public class Item {

    private long itemId;
    private long categoryId;

    private String itemName;
    private boolean status = false;

    public Item(long categoryId, String itemName) {
        this.categoryId = categoryId;
        this.itemName = itemName;
    }
    public Item(){}

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public String toString() {
        return "Item{" +
                "itemId=" + itemId +
                ", categoryId=" + categoryId +
                ", itemName='" + itemName + '\'' +
                ", status=" + status +
                '}';
    }
}
